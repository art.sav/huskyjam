<?php

use yii\db\Migration;

/**
 * Class m190321_144543_appinit
 */
class m190321_144543_appinit extends Migration
{
    private $tableOptions = '';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // carriers
        $this->createTable('{{%carriers}}', [
            'carrier_id' => $this->string(15)->notNull(),
            'carrier_name' => $this->string(255)->null(),
            'PRIMARY KEY (carrier_id)',
        ], $this->tableOptions);

        // routes
        $this->createTable('{{%routes}}', [
            'route_id' => $this->string(15)->notNull(),
            'station_departure_id' => $this->string(15)->notNull(),
            'time_departure' => $this->time()->notNull(),
            'station_arrival_id' => $this->string(15)->notNull(),
            'time_arrival' => $this->time()->notNull(),
            'time_route' => $this->integer(9)->unsigned()->null(),
            'price_ticket' => $this->decimal(15,2)->null(),
            'price_ticket_key' => $this->char(3)->null(),
            'carrier_id' => $this->string(15)->notNull(),
            'schedule' => "SET ('1', '2', '3', '4', '5', '6', '7') DEFAULT NULL",
            'PRIMARY KEY (route_id)',
        ], $this->tableOptions);

        // stations
        $this->createTable('{{%stations}}', [
            'station_id' => $this->string(15)->notNull(),
            'station_name' => $this->string(255)->notNull(),
            'PRIMARY KEY (station_id)',
        ], $this->tableOptions);

        //idx: routes
        $this->createIndex('idx_routes_carriers_id', '{{%routes}}', 'carrier_id');
        $this->createIndex('idx_routes_station_departure_id', '{{%routes}}', 'station_departure_id');
        $this->createIndex('idx_routes_station_arrival_id', '{{%routes}}', 'station_arrival_id');

        $this->addForeignKey('fk_carriers_id', '{{%routes}}', 'carrier_id', '{{%carriers}}', 'carrier_id', 'CASCADE', 'NO ACTION');
        $this->addForeignKey('fk_stations_id_depart', '{{%routes}}', 'station_departure_id', '{{%stations}}', 'station_id', 'RESTRICT', 'NO ACTION');
        $this->addForeignKey('fk_stations_id_arrival', '{{%routes}}', 'station_arrival_id', '{{%stations}}', 'station_id', 'RESTRICT', 'NO ACTION');

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190321_144543_appinit cannot be reverted.\n";

        $this->dropForeignKey('fk_stations_id_arrival', '{{%routes}}');
        $this->dropForeignKey('fk_stations_id_depart', '{{%routes}}');
        $this->dropForeignKey('fk_carriers_id', '{{%routes}}');

        $this->dropIndex('idx_routes_carriers_id', '{{%routes}}');
        $this->dropIndex('idx_routes_station_departure_id', '{{%routes}}');
        $this->dropIndex('idx_routes_station_arrival_id', '{{%routes}}');

        $this->dropTable('{{%stations}}');
        $this->dropTable('{{%carriers}}');
        $this->dropTable('{{%routes}}');

        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190321_144543_appinit cannot be reverted.\n";

        return false;
    }
    */
}
