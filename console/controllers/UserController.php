<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;
use \common\models\User;

class UserController extends Controller
{

    public function actionCreateOrUpdate($userLogin, $password = null)
    {

        $user = User::find()->where(['username' => $userLogin ])->one();

        if (!$user) {
            echo PHP_EOL."NEW USER".PHP_EOL;
            $user = new User();
            $user->username = $userLogin;
            $user->status = User::STATUS_ACTIVE;

            if(!$user->save()) {
                print_r($user->errors);
                return false;
            }
        }

        if ($user) {

            if ($password == null) {
                $password = \backend\components\Helpers::genPassword();
            }

            $user->setPassword($password);

            if ($user->save()) {
                $result['password'] = $password;
            }

            print_r(["username" => $user->username, "password" => $password]);

            if ($user->status == User::STATUS_DELETED) {
               print_r('User::STATUS_DELETED');
            }

        }

    }

}
