<?php

namespace common\tests\unit\models;

use Yii;
use common\models\Station;

/**
 * Login form test
 */
class StationTest extends \Codeception\Test\Unit
{
    /**
     * @var \common\tests\UnitTester
     */
    protected $tester;


    public function testCreateId()
    {
        $model = new Station();
        $model->station_name = 'test station';
        $this->assertTrue($model->save(), 'Save model');
        $this->assertNotEmpty($model->station_id, 'set station_id');
    }

}
