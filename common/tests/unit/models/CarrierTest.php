<?php

namespace common\tests\unit\models;

use Yii;
use common\models\Carrier;
/**
 * Login form test
 */
class CarrierTest extends \Codeception\Test\Unit
{
    /**
     * @var \common\tests\UnitTester
     */
    protected $tester;


    public function testCreateId()
    {
        $model = new Carrier();
        $model->carrier_name = 'test carrier';
        $this->assertTrue($model->save(), 'Save model');
        $this->assertNotEmpty($model->carrier_id, 'set carrier_id');
    }

}
