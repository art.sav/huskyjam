<?php

namespace common\models;

use Yii;
use backend\components\Guid\GuidFactory;

/**
 * This is the model class for table "stations".
 *
 * @property string $station_id
 * @property string $station_name
 *
 * @property Routes[] $routes
 * @property Routes[] $routes0
 */
class Station extends \yii\db\ActiveRecord
{
    const PREFIX_GUID = 'ST';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'stations';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['station_name'], 'required'],
            ['station_id', 'backend\components\Guid\GuidValidator'],
            [['station_name'], 'string', 'max' => 255],
            [['station_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'station_id' => 'ID',
            'station_name' => 'Название',
        ];
    }

    public function beforeSave($insert)
    {

        if (!parent::beforeSave($insert)) {
            return false;
        }

        if($insert) {
            $this->station_id = GuidFactory::make(Station::PREFIX_GUID);
        }

        return true;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoutes()
    {
        return $this->hasMany(Route::className(), ['station_arrival_id' => 'station_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoutes0()
    {
        return $this->hasMany(Route::className(), ['station_departure_id' => 'station_id']);
    }
}
