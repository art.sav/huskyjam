<?php

namespace common\models;

use Yii;
use backend\components\Guid\GuidFactory;

/**
 * This is the model class for table "routes".
 *
 * @property string $route_id
 * @property string $station_departure_id
 * @property string $time_departure
 * @property string $station_arrival_id
 * @property string $time_arrival
 * @property int $time_route
 * @property string $price_ticket
 * @property string $price_ticket_key
 * @property string $carrier_id
 * @property string $schedule
 *
 * @property Carriers $carrier
 * @property Stations $stationArrival
 * @property Stations $stationDeparture
 */
class Route extends \yii\db\ActiveRecord
{
    const PREFIX_GUID = 'RO';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'routes';
    }

    public function extraFields()
    {

        return ['carrier', 'stationArrival', 'stationDeparture'];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['station_departure_id', 'time_departure', 'station_arrival_id', 'time_arrival', 'carrier_id'], 'required'],
            [['time_departure', 'time_arrival'], 'match', 'pattern' => '/^([0-9]|0[0-9]|1[0-9]|2[0-3])\:[0-5][0-9](\:[0-5][0-9]|)$/'],
            ['route_id', 'backend\components\Guid\GuidValidator'],
            [['time_route'], 'integer'],
            [['price_ticket'], 'number'],
            [['station_departure_id', 'station_arrival_id', 'carrier_id'], 'string', 'max' => 15],
            [['price_ticket_key'], 'string', 'max' => 3],
            [['route_id'], 'unique'],
            [['schedule'], 'in', 'range' => array_keys(\Yii::$app->params['dayList']), 'allowArray' => true ],
            [['carrier_id'], 'exist', 'skipOnError' => true, 'targetClass' => Carrier::className(), 'targetAttribute' => ['carrier_id' => 'carrier_id']],
            [['station_arrival_id'], 'exist', 'skipOnError' => true, 'targetClass' => Station::className(), 'targetAttribute' => ['station_arrival_id' => 'station_id']],
            [['station_departure_id'], 'exist', 'skipOnError' => true, 'targetClass' => Station::className(), 'targetAttribute' => ['station_departure_id' => 'station_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'route_id' => 'Route ID',
            'station_departure_id' => 'Станция отправления',
            'time_departure' => 'Время отправления',
            'station_arrival_id' => 'Станция прибытия',
            'time_arrival' => 'Время прибытия',
            'time_route' => 'Время в пути (минут)',
            'price_ticket' => 'Цена билета',
            'price_ticket_key' => 'Валюта',
            'carrier_id' => 'Перевозчик',
            'schedule' => 'Дни недели отправления',
        ];
    }


    public function beforeSave($insert)
    {

        if (!parent::beforeSave($insert)) {
            return false;
        }

        if($insert) {
            $this->route_id = GuidFactory::make(Route::PREFIX_GUID);
        }

        return true;
    }


    public function __get($name)
    {
        if($name == 'schedule') {
            if(is_string($this->getAttribute('schedule'))) {
                return explode(',',$this->getAttribute('schedule'));
            }
        }

        return parent::__get($name);
    }

    public function __set($name, $value)
    {
        if($name == 'schedule') {
            if(is_array($value)) {
                $value = implode(',',$value);
            }
        }

        $res = parent::__set($name, $value);
        return $res;
    }

    public function toArray ( array $fields = [], array $expand = [], $recursive = true )
    {
        $array = parent::toArray($fields, $expand, $recursive);
        $array['schedule'] = implode(',',$array['schedule']);
        return $array;
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarrier()
    {
        return $this->hasOne(Carrier::className(), ['carrier_id' => 'carrier_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStationArrival()
    {
        return $this->hasOne(Station::className(), ['station_id' => 'station_arrival_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStationDeparture()
    {
        return $this->hasOne(Station::className(), ['station_id' => 'station_departure_id']);
    }
}
