<?php

namespace common\models;

use Yii;
use backend\components\Guid\GuidFactory;

/**
 * This is the model class for table "carriers".
 *
 * @property string $carrier_id
 * @property string $carrier_name
 *
 * @property Routes[] $routes
 */
class Carrier extends \yii\db\ActiveRecord
{
    const PREFIX_GUID = 'CA';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'carriers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['carrier_name'], 'required'],
            [['carrier_name'], 'string', 'max' => 255],
            ['carrier_id', 'backend\components\Guid\GuidValidator'],
            [['carrier_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'carrier_id' => 'ID',
            'carrier_name' => 'Название',
        ];
    }


    public function beforeSave($insert)
    {

        if (!parent::beforeSave($insert)) {
            return false;
        }

        if($insert) {
            $this->carrier_id = GuidFactory::make(Carrier::PREFIX_GUID);
        }

        return true;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoutes()
    {
        return $this->hasMany(Route::className(), ['carrier_id' => 'carrier_id']);
    }
}
