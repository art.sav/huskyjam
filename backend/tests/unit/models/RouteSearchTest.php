<?php

namespace backend\tests\unit\models;

use Yii;
use \common\models\{Route, Station, Carrier};
use \backend\models\RouteSearch;
use yii\data\ActiveDataProvider;

/**
 * RouteSearchTest form test
 */
class RouteSearchTest extends \Codeception\Test\Unit
{
    /**
     * @var \common\tests\UnitTester
     */
    protected $tester;


    public function testSearch()
    {
        $modelStation = new Station();
        $modelStation->station_name = 'test station';
        $modelStation->save();

        $modelStation2 = new Station();
        $modelStation2->station_name = 'test station2';
        $modelStation2->save();

        $modelCarrier = new Carrier();
        $modelCarrier->carrier_name = 'test carrier';
        $modelCarrier->save();

        $modelRoute = new Route();

        $modelRoute->station_departure_id = $modelStation->station_id;
        $modelRoute->time_departure = '20:09';
        $modelRoute->station_arrival_id = $modelStation2->station_id;
        $modelRoute->time_arrival = '12:02';
        $modelRoute->carrier_id = $modelCarrier->carrier_id;
        $modelRoute->time_route = 144;
        $modelRoute->price_ticket = 100;
        $modelRoute->price_ticket_key = 'RUB';
        $modelRoute->schedule = [ 3, 6, 7];
        $modelRoute->save();

        $RouteSearch = new RouteSearch();
        $res = $RouteSearch->search(['route_id' => $modelRoute->route_id]);

        $this->assertInstanceOf( ActiveDataProvider::class, $res );
        $this->assertInstanceOf( Route::class, $res->getModels()[0] );

    }

}
