<?php

namespace backend\tests\unit\models;

use Yii;
use \common\models\Station;
use \backend\models\StationSearch;
use yii\data\ActiveDataProvider;

/**
 * StationSearchTest form test
 */
class StationSearchTest extends \Codeception\Test\Unit
{
    /**
     * @var \common\tests\UnitTester
     */
    protected $tester;


    public function testSearch()
    {

        $model = new Station();
        $model->station_name = 'test station';
        $model->save();

        $CarrierSearch = new StationSearch();
        $res = $CarrierSearch->search(['station_id' => $model->station_id]);

        $this->assertInstanceOf( ActiveDataProvider::class, $res );
        $this->assertInstanceOf( Station::class, $res->getModels()[0] );

    }

}
