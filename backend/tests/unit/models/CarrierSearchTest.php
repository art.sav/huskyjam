<?php

namespace backend\tests\unit\models;

use Yii;
use \common\models\Carrier;
use \backend\models\CarrierSearch;
use yii\data\ActiveDataProvider;

/**
 * CarrierSearchTest form test
 */
class CarrierSearchTest extends \Codeception\Test\Unit
{
    /**
     * @var \common\tests\UnitTester
     */
    protected $tester;


    public function testSearch()
    {

        $model = new Carrier();
        $model->carrier_name = 'test carrier';
        $model->save();

        $CarrierSearch = new CarrierSearch();
        $res = $CarrierSearch->search(['carrier_id' => $model->carrier_id]);

        $this->assertInstanceOf( ActiveDataProvider::class, $res );
        $this->assertInstanceOf(Carrier::class, $res->getModels()[0]);

    }

}
