<?php

namespace backend\tests\unit\components;

use Yii;
use backend\components\Helpers;

/**
 * HelpersTest form test
 */
class HelpersTest extends \Codeception\Test\Unit
{
    /**
     * @var \common\tests\UnitTester
     */
    protected $tester;


    public function testGenPassword()
    {

        $password = Helpers::genPassword(15);
        $this->assertRegExp('/[a-z0-9]{15}/i', $password, 'gen password');
    }

}
