<?php

namespace backend\tests\unit\components\Guid;

use Yii;

use backend\components\Guid\GuidFactory;

/**
 * GuidFactoryTest form test
 */
class GuidFactoryTest extends \Codeception\Test\Unit
{
    /**
     * @var \common\tests\UnitTester
     */
    protected $tester;


    public function testMake()
    {

        $uid = GuidFactory::make('TE');
        $this->assertRegExp('/TE[a-z0-9]{13}/', $uid, 'Make uid');
    }

}
