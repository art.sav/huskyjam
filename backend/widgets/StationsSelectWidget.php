<?php

namespace backend\widgets;

use yii\base\Widget;
use \common\models\Station;
use \yii\helpers\ArrayHelper;

class StationsSelectWidget extends Widget
{
    public $form;
    public $model;
    public $name_input;

    public function init()
    {
        parent::init();

        if ($this->form === null) {
            throw new \Exception("need form", 1);
        }

        if ($this->model === null) {
            throw new \Exception("need model", 1);
        }

        if ($this->name_input === null) {
            throw new \Exception("need name", 1);
        }
    }

    public function run()
    {
        $stationList = Station::find()->select(['station_id', 'station_name'])->asArray()->all();

        $stationListDropDown = ArrayHelper::map($stationList, 'station_id', 'station_name');

        return $this->form->field($this->model, $this->name_input)->dropDownList($stationListDropDown, ['prompt' => 'Выберите станцию']);
    }
}
