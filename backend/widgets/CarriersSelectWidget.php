<?php

namespace backend\widgets;

use yii\base\Widget;
use \common\models\Carrier;
use \yii\helpers\ArrayHelper;

class CarriersSelectWidget extends Widget
{
    public $form;
    public $model;
    public $name_input;

    public function init()
    {
        parent::init();

        if ($this->form === null) {
            throw new \Exception("need form", 1);
        }

        if ($this->model === null) {
            throw new \Exception("need model", 1);
        }

        if ($this->name_input === null) {
            throw new \Exception("need name", 1);
        }
    }

    public function run()
    {
        $carriersList = Carrier::find()->asArray()->all();

        $carriersListDropDown = ArrayHelper::map($carriersList, 'carrier_id', 'carrier_name');

        return $this->form->field($this->model, $this->name_input)->dropDownList($carriersListDropDown, ['prompt' => 'Выберите перевозчика']);
    }
}
