<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-backend',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'stationService' => [
            'class' => 'backend\Services\StationService'
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => [

                '' => 'site/index',
                'site/login' => 'site/login',
                'site/logout' => 'site/logout',

                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => [ 'carrier' => 'carrier-rest' ],
                    'prefix' => 'api/v1',
                    'tokens' => [ '{carrier_id}' => '<id:(CA[0-9a-zA-Z]{13})>' ],
                    'patterns' => ['PUT,PATCH {carrier_id}' => 'update', 'DELETE {carrier_id}' => 'delete', 'GET,HEAD {carrier_id}' => 'view', 'POST' => 'create', 'GET,HEAD' => 'index', '{carrier_id}' => 'options', '' => 'options']
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => [ 'station' => 'station-rest' ],
                    'prefix' => 'api/v1',
                    'tokens' => [ '{station_id}' => '<id:(ST[0-9a-zA-Z]{13})>' ],
                    'patterns' => ['PUT,PATCH {station_id}' => 'update', 'DELETE {station_id}' => 'delete', 'GET,HEAD {station_id}' => 'view', 'POST' => 'create', 'GET,HEAD' => 'index', '{station_id}' => 'options', '' => 'options']
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => [ 'route' => 'route-rest' ],
                    'prefix' => 'api/v1',
                    'tokens' => [ '{route_id}' => '<id:(RO[0-9a-zA-Z]{13})>' ],
                    'patterns' => ['PUT,PATCH {route_id}' => 'update', 'DELETE {route_id}' => 'delete', 'GET,HEAD {route_id}' => 'view', 'POST' => 'create', 'GET,HEAD' => 'index', '{route_id}' => 'options', '' => 'options']
                ],

                'GET stations' => 'station/list',
                'GET station/<station_id:([0-9a-zA-Z]{15})>/view' => 'station/view',
                'station/<station_id:([0-9a-zA-Z]{15})>/update' => 'station/update',
                'station/<station_id:([0-9a-zA-Z]{15})>/delete' => 'station/delete',

                'GET carriers' => 'carrier/list',
                'GET carrier/<carrier_id:([0-9a-zA-Z]{15})>/view' => 'carrier/view',
                'carrier/<carrier_id:([0-9a-zA-Z]{15})>/update' => 'carrier/update',
                'carrier/<carrier_id:([0-9a-zA-Z]{15})>/delete' => 'carrier/delete',

                'GET routes' => 'route/list',
                'GET route/<route_id:([0-9a-zA-Z]{15})>/view' => 'route/view',
                'route/<route_id:([0-9a-zA-Z]{15})>/update' => 'route/update',
                'route/<route_id:([0-9a-zA-Z]{15})>/delete' => 'route/delete',
            ],
        ],
    ],
    'params' => $params,
];
