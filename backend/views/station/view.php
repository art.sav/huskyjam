<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Station */

$this->title = 'Station';
$this->params['breadcrumbs'][] = ['label' => 'Stations', 'url' => ['list']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="station-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'station_id' => $model->station_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'station_id' => $model->station_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
                            'station_id:ntext',
                            'station_name:ntext',
                        ],
    ])
    ?>

</div>
