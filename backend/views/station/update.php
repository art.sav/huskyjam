<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Station */

$this->title = 'Station';
$this->params['breadcrumbs'][] = ['label' => 'Stations', 'url' => ['list']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="station-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
