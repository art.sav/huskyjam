<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Route */

$this->title = 'Route';
$this->params['breadcrumbs'][] = ['label' => 'Routes', 'url' => ['list']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
$formatter = \Yii::$app->formatter;

?>
<div class="route-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'route_id' => $model->route_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'route_id' => $model->route_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'label' => 'Станция отправления',
                'value' => function ($model){
                    if($station = $model->stationDeparture) {
                            return $station->station_name;
                    }
                    return '';
                }
            ],
            'time_departure:ntext',
            [
                'label' => 'Станция прибытия',
                'value' => function ($model){
                    if($station = $model->stationArrival) {
                            return $station->station_name;
                    }
                    return '';
                }
            ],
            'time_arrival:ntext',
            'time_route:ntext',
            [
                'label' => 'Перевозчик',
                'value' => function ($model){
                    if($carrier = $model->carrier) {
                            return $carrier->carrier_name;
                    }
                    return '';
                }
            ],
            [
                'label' => 'Цена',
                'value' => function($model) use ($formatter) {
                    return $formatter->asCurrency($model->price_ticket, $model->price_ticket_key);
                }
            ],
            [
                'label' => 'Расписание',
                'value' => function($model) {
                    $listDays = array_intersect_key(\Yii::$app->params['dayList'], array_flip($model->schedule));
                    return implode(', ',$listDays);
                }
            ]
        ]
    ]) ?>

</div>
