<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Route */

$this->title = 'Route';
$this->params['breadcrumbs'][] = ['label' => 'Routes', 'url' => ['list']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="route-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model
    ]) ?>

</div>
