<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\RouteSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="route-search">

    <?php $form = ActiveForm::begin([
        'action' => ['list'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'route_id') ?>

    <?= $form->field($model, 'station_departure_id') ?>

    <?= $form->field($model, 'time_departure') ?>

    <?= $form->field($model, 'station_arrival_id') ?>

    <?= $form->field($model, 'time_arrival') ?>

    <?php // echo $form->field($model, 'time_route') ?>

    <?php // echo $form->field($model, 'price_ticket') ?>

    <?php // echo $form->field($model, 'price_ticket_key') ?>

    <?php // echo $form->field($model, 'carrier_id') ?>

    <?php // echo $form->field($model, 'schedule') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
