<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $providerList yii\data\ArrayDataProvider */

$this->title = 'Routes';
$this->params['breadcrumbs'][] = $this->title;
$formatter = \Yii::$app->formatter;

?>

<div class="route-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Route', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => 'Станция отправления',
                'value' => function ($model){
                    if($station = $model->stationDeparture) {
                            return $station->station_name;
                    }
                    return '';
                }
            ],
            'time_departure:ntext',
            [
                'label' => 'Станция прибытия',
                'value' => function ($model){
                    if($station = $model->stationArrival) {
                            return $station->station_name;
                    }
                    return '';
                }
            ],
            'time_arrival:ntext',
            'time_route:ntext',
            [
                'label' => 'Перевозчик',
                'value' => function ($model){
                    if($carrier = $model->carrier) {
                            return $carrier->carrier_name;
                    }
                    return '';
                }
            ],
            [
                'label' => 'Цена',
                'value' => function($model) use ($formatter) {
                    return $formatter->asCurrency($model->price_ticket, $model->price_ticket_key);
                }
            ],
            [
                'label' => 'Расписание',
                'value' => function($model) {
                    $listDays = array_intersect_key(\Yii::$app->params['dayList'], array_flip($model->schedule));
                    return implode(', ',$listDays);
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'urlCreator' => function ($action, $model, $key, $index, $t) {
                    return Url::toRoute([$action, 'route_id' => $model->route_id]);
                }
            ]
        ],
    ]); ?>

</div>
