<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \backend\widgets\StationsSelectWidget;
use \backend\widgets\CarriersSelectWidget;

/* @var $this yii\web\View */
/* @var $model common\models\Route */
/* @var $form yii\widgets\ActiveForm */
/* @var $carriersList */
/* @var $stationList */

?>

<div class="route-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= StationsSelectWidget::widget([ 'form' => $form, 'model' => $model, 'name_input' => 'station_departure_id' ]) ?>

    <?= $form->field($model, 'time_departure')->textInput(['type' => 'time']) ?>

    <?= StationsSelectWidget::widget([ 'form' => $form, 'model' => $model, 'name_input' => 'station_arrival_id' ]) ?>

    <?= $form->field($model, 'time_arrival')->textInput(['type' => 'time']) ?>

    <?= $form->field($model, 'time_route')->textInput() ?>

    <?= CarriersSelectWidget::widget([ 'form' => $form, 'model' => $model, 'name_input' => 'carrier_id' ]) ?>

    <?= $form->field($model, 'price_ticket')->textInput(['type' => 'number']) ?>

    <?= $form->field($model, 'price_ticket_key')->dropDownList(\Yii::$app->params['currencyList']) ?>

    <?= $form->field($model, 'schedule')->dropDownList(\Yii::$app->params['dayList'], ['multiple' => 'multiple', 'size' => 7]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
