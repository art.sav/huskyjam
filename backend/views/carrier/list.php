<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $providerList yii\data\ArrayDataProvider */

$this->title = 'Carriers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="carrier-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Carrier', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'carrier_id:ntext',
            'carrier_name:ntext',
            [
                'class' => 'yii\grid\ActionColumn',
                'urlCreator' => function ($action, $model, $key, $index, $t) {
                    return Url::toRoute([$action, 'carrier_id' => $model->carrier_id]);
                }
            ],
        ],
    ]); ?>

</div>
