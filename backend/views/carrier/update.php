<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Carrier */

$this->title = 'Carrier';
$this->params['breadcrumbs'][] = ['label' => 'Carriers', 'url' => ['list']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="carrier-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
