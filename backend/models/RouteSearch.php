<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Route;

/**
 * RouteSearch represents the model behind the search form of `common\models\Route`.
 */
class RouteSearch extends Route
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['route_id', 'station_departure_id', 'time_departure', 'station_arrival_id', 'time_arrival', 'price_ticket_key', 'carrier_id', 'schedule'], 'safe'],
            [['time_route'], 'integer'],
            [['price_ticket'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Route::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'time_departure' => $this->time_departure,
            'time_arrival' => $this->time_arrival,
            'time_route' => $this->time_route,
            'price_ticket' => $this->price_ticket,
            'route_id' => $this->route_id,
        ]);

        $query->andFilterWhere(['like', 'station_departure_id', $this->station_departure_id])
              ->andFilterWhere(['like', 'station_arrival_id', $this->station_arrival_id])
              ->andFilterWhere(['like', 'price_ticket_key', $this->price_ticket_key])
              ->andFilterWhere(['like', 'carrier_id', $this->carrier_id])
              ->andFilterWhere(['like', 'schedule', $this->schedule]);

        return $dataProvider;
    }
}
