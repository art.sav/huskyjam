<?php

namespace backend\components;

use Yii;

class Helpers
{

   public static function genPassword($length=15)
   {
	      $chars="qazxswedcvfrtgbnhyujmkiolp1234567890QAZXSWEDCVFRTGBNHYUJMKIOLP";
	      $length = intval($length);
	      $size=strlen($chars)-1;
	      $password = "";
	      while($length--) $password.=$chars[rand(0,$size)];
	      return $password;
   }

}
