<?php

namespace backend\components\Guid;


class GuidValidator extends \yii\validators\Validator
{

    const prefix = ['ST', 'RO', 'CA'];

    public function validateAttribute($model, $attribute)
    {
		  if(is_array($model->$attribute)){
			  foreach ($model->$attribute as $key => $value) {
   			  if(!self::check($value)){
   				  $this->addError($model, $attribute, 'Должно быть масивом ID '.$this->getModel($model, $attribute));
   			  }
			  }
		  }else{
			  if(!self::check($model->$attribute)){
				  $this->addError($model, $attribute, 'Должно быть ID '.$this->getModel($model, $attribute));
			  }
		  }
    }

    private function getModel($model,$attribute)
    {

      $modelClass = get_class($model);
      $modelClass = preg_match('/(?<class>[a-z0-9-]*)$/i',$modelClass,$match);
      if($modelClass) {
         $model = $match['class'];
      }

      return sprintf('(%s %s)',$model,$attribute);

    }

    public static function check(string $guid): bool
    {
         if(strlen($guid) == 15){
            $prefix = substr($guid,0,2);
            if(in_array($prefix,self::prefix) && preg_match('/^[a-zA-Z0-9]*$/i',$guid)){
               return true;
            }
         }

         return false;
    }

}

?>
