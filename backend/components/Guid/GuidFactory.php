<?php

namespace backend\components\Guid;


class GuidFactory
{

   static public function make($prefix = 'XX')
   {
      return uniqid($prefix);
   }

}
