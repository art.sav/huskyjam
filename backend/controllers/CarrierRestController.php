<?php

namespace backend\controllers;


/**
 * CarrierRestController implements the CRUD actions for Carrier model.
 */
class CarrierRestController extends RestBaseController
{
    public $modelClass = 'common\models\Carrier';
}
