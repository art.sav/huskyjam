<?php

namespace backend\controllers;

use Yii;


/**
 * RouteController implements the CRUD actions for Route model.
 */
class RouteController extends CrudBaseController
{

    const ModelClass = 'common\models\Route';
    const SearchModelClass = 'backend\models\RouteSearch';
    const PK_KEY = 'route_id';

}
