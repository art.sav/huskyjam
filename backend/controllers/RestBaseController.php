<?php

namespace backend\controllers;

use Yii;

use yii\rest\ActiveController;

/**
 * RestBaseController implements the CRUD actions for Route model.
 */
class RestBaseController extends ActiveController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['access'] = [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ];

        return $behaviors;
    }
}
