<?php

namespace backend\controllers;

use Yii;
use common\models\Route;
use backend\models\RouteSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CrudController implements the CRUD actions for Route model.
 */
class CrudBaseController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function getConstant(string $class)
    {
        return constant(sprintf('%s::%s', get_called_class(), $class));
    }

    /**
     * Lists all Station models.
     * @return mixed
     */
    public function actionList()
    {
        $modelPath = $this->getConstant('SearchModelClass');

        $searchModel = new $modelPath();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('list', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Station model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView()
    {
        $id = \Yii::$app->request->get($this->getConstant('PK_KEY'));
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Station model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $modelPath = $this->getConstant('ModelClass');

        $model = new $modelPath();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', $this->getConstant('PK_KEY') => $model->{$this->getConstant('PK_KEY')}]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Station model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate()
    {
        $id = \Yii::$app->request->get($this->getConstant('PK_KEY'));
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', $this->getConstant('PK_KEY') => $model->{$this->getConstant('PK_KEY')}]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Station model.
     * If deletion is successful, the browser will be redirected to the 'list' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete()
    {
        $id = \Yii::$app->request->get($this->getConstant('PK_KEY'));
        $this->findModel($id)->delete();

        return $this->redirect(['list']);
    }

    /**
     * Finds the Station model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Station the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {

        $modelPath = $this->getConstant('ModelClass');

        //$model = new $modelPath();

        if (($model = $modelPath::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
