<?php

namespace backend\controllers;


/**
 * CarrierController implements the CRUD actions for Carrier model.
 */
class CarrierController extends CrudBaseController
{

    const ModelClass = 'common\models\Carrier';
    const SearchModelClass = 'backend\models\CarrierSearch';
    const PK_KEY = 'carrier_id';

}
