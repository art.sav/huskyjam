<?php

namespace backend\controllers;


/**
 * CarrierRestController implements the CRUD actions for Route model.
 */
class RouteRestController extends RestBaseController
{
    public $modelClass = 'common\models\Route';
}
