<?php

namespace backend\controllers;

use Yii;

/**
 * StationController implements the CRUD actions for Station model.
 */
class StationController extends CrudBaseController
{

    const ModelClass = 'common\models\Station';
    const SearchModelClass = 'backend\models\StationSearch';
    const PK_KEY = 'station_id';

}
