<?php

namespace backend\controllers;


/**
 * CarrierRestController implements the CRUD actions for Station model.
 */
class StationRestController extends RestBaseController
{
    public $modelClass = 'common\models\Station';
}
