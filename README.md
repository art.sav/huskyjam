
Step 1:
    create db "tutu"
    create db "tutu_test" for tests

Step 2:
    composer install

Step 3:
    ./init

Step 4:
    edit @common/config/main-local.php
    edit @common/config/test-local.php

Step 5:
    ./yii migrate

Step 6:
    ./yii user/create-or-update LOGIN PASSWORD


for test: ./vendor/bin/codecept run


Local start:
    ./yii serve -t @backend/web -p 8080


API:

    Getting routes:
        GET /api/v1/route?expand=carrier,stationArrival,stationDeparture
        GET /api/v1/route/<ROUTE_ID>?expand=carrier,stationArrival,stationDeparture

    Create route:
        POST /api/v1/route

    Update route:
        PATCH, PUT /api/v1/route/<ROUTE_ID>

    Delete route:
        DELETE /api/v1/route/<ROUTE_ID>


    Getting stations:
        GET /api/v1/station

    Create station:
        POST /api/v1/station

    Update station:
        PATCH, PUT /api/v1/station/<STATION_ID>

    Delete station:
        DELETE /api/v1/station/<STATION_ID>


    Getting carriers:
        GET /api/v1/carrier

    Create carrier:
        POST /api/v1/carrier

    Update carrier:
        PATCH, PUT /api/v1/carrier/<CARRIER_ID>

    Delete carrier:
        DELETE /api/v1/carrier/<CARRIER_ID>
